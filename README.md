# pomd

pomd is a command line pomodoro tool implemented as an Unix daemon.
It consists of the daemon (pomd) and the controller (pomc).

## Building/ Installation

This program is written in Rust and can be compiled and installed using the standard Rust tool chain. You can find Rust installation information for your OS on [Rust's official site](https://www.rust-lang.org/). 

To build:

```
$ git clone https://gitlab.com/cosmo_duff/pomd.git && cd pomd
$ cargo build --release
```

To install:

```
$ git clone https://gitlab.com/cosmo_duff/pomd.git && cd pomd
$ cargo install --path ./
```

This will place a compiled binary in $HOME/.cargo/bin on Unix and Unix like systems. 

## Usage

pomd:

```
pomd 0.1.0

USAGE:
    pomd [FLAGS] [OPTIONS]

FLAGS:
    -F, --foreground       Run the process in the foreground
    -h, --help             Prints help information
    -n, --notifications    Enables notifications
    -V, --version          Prints version information

OPTIONS:
    -b, --break <BREAK>              Break length in minutes [default: 5]
    -B, --long-break <LONG BREAK>    Long break length in minutes [default: 15]
    -p, --pomodoro <POMODORO>        Pomodoro length in minutes [default: 25]
    -r, --rounds <ROUNDS>            Number of rounds before a long break [default: 4]

```

pomc:

```
pomc 0.1.0

USAGE:
    pomc [FLAGS] <ACTION>

FLAGS:
    -h, --help       Prints help information
    -j, --json       Get the status in json format
    -V, --version    Prints version information

ARGS:
    <ACTION>    Action to send to pomd [possible values: play, pause, stop, toggle,
                status]

```

## Runnning

Start the dameon with desktop notifications:

`$ pomd -n`

Start the pomodoro:

`$ pomc play`

Get the status of the pomodoro:

`$ pomc status`

Get the status as json:

`$ pomc -j status`

## libpom

All of the pomodoro code is handled by libpom. It can be found in this repo [here](./libpom).
