use common::error::PomdError;
use libpom::{Pomodoro, State};
use log::{debug, error, info};
use notify_rust::Notification;

use std::{
    os::unix::net::UnixDatagram, path::Path, str, sync::mpsc::Receiver, thread, thread::JoinHandle,
};

type Result<T> = std::result::Result<T, PomdError>;

pub(crate) fn handle_notifications(receiver: Receiver<State>) -> JoinHandle<()> {
    thread::spawn(move || loop {
        let state = receiver.recv();
        match state {
            Ok(s) => {
                let notification = format!("Begin {}", s.phase());
                Notification::new()
                    .summary("Pomodoro")
                    .body(&notification)
                    .sound_name("message-new-instant")
                    .show()
                    .unwrap();
                info!("Sent notification.");
            }
            Err(e) => error!("Unable to receive state: {}", e),
        }
    })
}

pub(crate) fn handle_message<'a>(
    pom: &'a mut Pomodoro,
    buf: &'a [u8],
    server: &UnixDatagram,
    client: &'a Path,
) -> Result<()> {
    // convert buffer to str
    let message = str::from_utf8(buf).unwrap().trim_matches(char::from(0));

    match message {
        "play" => {
            if let Err(e) = pom.play() {
                return Err(PomdError::PlayError(e));
            };
            info!("Executed play command.");
        }

        "pause" => {
            info!("Executing pause command.");
            pom.pause();
        }

        "stop" => {
            info!("Executing stop command.");
            pom.stop();
        }

        "toggle" => {
            if let Err(e) = pom.toggle() {
                return Err(PomdError::PlayError(e));
            };
            info!("Executed toggle command.");
        }

        "status" => {
            let status = format!("{}", pom.status());
            send_status(server, client, status)?;
        }
        "json" => {
            let status = match serde_json::to_string(&pom.status()) {
                Ok(s) => s,
                Err(e) => return Err(PomdError::StatusDeserializeFailed(e)),
            };
            send_status(server, client, status)?;
        }

        _ => {
            error!("Unknown command");
        }
    }

    Ok(())
}

fn send_status(server: &UnixDatagram, client: &Path, status: String) -> Result<()> {
    info!("Sending status: {}", status);
    if let Err(e) = server.send_to(status.as_bytes(), client) {
        return Err(PomdError::SocketSendError(e));
    };
    debug!("Executed the status command.");
    Ok(())
}
