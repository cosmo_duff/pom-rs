#[cfg(feature = "serde")]
use serde_crate::{Deserialize, Serialize};

use std::fmt;

#[derive(Debug, Copy, Clone, PartialEq)]
#[cfg_attr(
    feature = "serde",
    derive(Serialize, Deserialize),
    serde(crate = "serde_crate")
)]
/// An enum representing the different phases of a pomodoro (Pomodoro, Break, Long Break).
pub enum Phase {
    Pomodoro,
    Break,
    LongBreak,
}

impl fmt::Display for Phase {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let phase = match *self {
            Phase::Pomodoro => String::from("Pomodoro"),
            Phase::Break => String::from("Break"),
            Phase::LongBreak => String::from("Long Break"),
        };

        write!(f, "{}", phase)
    }
}
